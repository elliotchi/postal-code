# Postal Code CLI

Checks how many requests with postal codes are successful

## Getting Started

    npm install
    node postalCode.js [filter option in ''] [filter parameter in '']
 
## Filter Options
- 'Postal Code'
- 'Place Name'
- 'State'
- 'State Abbreviation'
- 'County'
- 'Latitude'
- 'Longitude'

### Example
    node postalCode.js 'State Abbreviation' 'CA'