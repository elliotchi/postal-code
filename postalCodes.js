const Converter = require('csvtojson').Converter;
const converter = new Converter({});
const pify = require('pify');
const request = require('request');
const throat = require('throat');


const buildURL = (postalCode) => `https://api.cleanify.com/suppliers/v1/quote?postalCode=${postalCode}`;

const pKey = 'Postal Code';

let success = 0;
let failure = 0;
let row = 0;

const onError = (err) => {
  console.log('Error: ' + err + ', Row: ' + ++row, 'Successful: ' + success, 'Failure: ' + failure);
  failure += 1;
}

const onSuccess = (response) => {
  if (response.statusCode > 400) {
    failure += 1;
    console.log('Error: ' + response.statusCode + ', Row: ' + ++row, 'Successful: ' + success, 'Failure: ' + failure);
  } else if (response.statusCode === 200) {
    success += 1;
    console.log('Success: ' + response.statusCode + ', Row: ' + ++row, 'Successful: ' + success, 'Failure: ' + failure);
  }
}

let createPromises = (result) =>
  result.map(
    value => value[pKey]
  )
  .map(
    pCode => (
      throat(3, pify(request))(buildURL(pCode)).then(onSuccess).catch(onError)
    )
  );

if (process.argv[2] && process.argv[3]) {
  createPromises = (result) => {
    const postalCodeRequests = result
      .filter(row => row[process.argv[2]] === process.argv[3])
      .map(value => value[pKey])
      .map(
        pCode => (
          throat(3, pify(request))(buildURL(pCode)).then(onSuccess).catch(onError)
        )
      )

      console.log('Searching ' + postalCodeRequests.length + ' requests for ' + process.argv[2] + ' = ' + process.argv[3]);
      return postalCodeRequests;
  }
}

converter.on('end_parsed', result => {
  console.log('Beginning postal code test, please wait');
  Promise.all(
    createPromises(result)
  )
  .then(() => {
    console.log('Successes: ' + success + ', ' + 'Failures: ' + failure);
    process.exit();
  });
});

require('fs').createReadStream('./us_postal_codes.csv').pipe(converter);